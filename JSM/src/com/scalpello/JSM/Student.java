package com.scalpello.JSM;

import java.io.IOException;
import java.util.Map;

import com.mongodb.*;
import com.mongodb.util.JSON;

import org.bson.types.ObjectId;
import com.sun.net.httpserver.*;

public class Student implements HttpHandler {

	public void handle(HttpExchange app) throws IOException {
		Auth.check(app);
		String method = app.getRequestMethod();
		Map <String,String> params = Utils.parseParams(app.getRequestURI().getQuery());

		DB db;
		DBCollection coll = null;
		try {
			db = Mongo.db();
			coll = db.getCollection("students");
		} catch (Exception e) {
			Utils.sendRes(app, 500);
		}

		if (method.equals("GET")) {
			if(params.get("id") != null) {
				String id = params.get("id");
				ObjectId objId = null;
				try {
					objId = new ObjectId(id);
				} catch (Exception e) {
					Utils.sendRes(app, 400);
				}
				BasicDBObject query = new BasicDBObject();
			    query.put("_id", objId);
			    DBObject res = coll.findOne(query);
			    if (res == null) {
			    	Utils.sendRes(app, 400);
			    } else {
			    	Utils.sendRes(app, res.toString());
			    }
			} else {
				String res = "[";
				DBCursor cursor = coll.find();
				try {
				   while(cursor.hasNext()) {
				       res += cursor.next() + ", ";
				   }
				} finally {
				   cursor.close();
				   if (res.length() > 1) {
					   res = res.substring(0, (res.length()-2));
				   }
				   res += "]";
				}
				Utils.sendRes(app, res);
			}
		} else if (method.equals("POST")) {
			String body = Utils.streamToString(app.getRequestBody()).trim();
			if (body.length() < 1 || !body.substring(0, 1).equals("{")) {
				Utils.sendRes(app, 400);
			} else {
				DBObject item = null;
				try {
					item = (DBObject) JSON.parse(body);
					coll.insert(item);
				} catch (Exception e) {
					//System.out.println("Error Creating Item");
					Utils.sendRes(app, 400);
					return;
				}
				Utils.sendRes(app, item.toString());
			}
		} else if (method.equals("PUT")) {
			if(params.get("id") != null) {
				String id = params.get("id");
				ObjectId objId = null;
				try {
					objId = new ObjectId(id);
				} catch (Exception e) {
					Utils.sendRes(app, 400);
				}
				BasicDBObject query = new BasicDBObject();
			    query.put("_id", objId);
			    String body = Utils.streamToString(app.getRequestBody()).trim();
				if (body.length() < 1 || !body.substring(0, 1).equals("{")) {
					Utils.sendRes(app, 400);
				} else {
					DBObject item = null;
					try {
						item = (DBObject) JSON.parse(body);
						coll.update(query, new BasicDBObject().append("$set", item));
					} catch (Exception e) {
						Utils.sendRes(app, 400);
						return;
					}
					Utils.sendRes(app, 200);
				}
			} else {
				Utils.sendRes(app, 400);
			}
		} else if (method.equals("DELETE")) {
			if(params.get("id") != null) {
				String id = params.get("id");
				try {
					ObjectId objId = new ObjectId(id);
					BasicDBObject query = new BasicDBObject();
				    query.put("_id", objId);
				    coll.remove(query);
				    Utils.sendRes(app, 200);
				} catch (Exception e) {
					Utils.sendRes(app, 400);
				}
			} else {
				Utils.sendRes(app, 400);
			}
		} else {
			Utils.sendRes(app, 400);
		}

	}
}
