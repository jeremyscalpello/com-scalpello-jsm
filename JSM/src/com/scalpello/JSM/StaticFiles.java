package com.scalpello.JSM;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

class StaticFiles implements HttpHandler {
    public void handle (HttpExchange app) throws IOException {
        String root = "./ui";
        URI uri = app.getRequestURI();
        String path = uri.getPath();
        System.out.println(path);
        if (!path.equals("/login.html") && (path.length() >= 4 && !path.substring(0, 4).equals("/lib")) || path.equals("/")) {
        	Auth.check(app);
        }
        if (path.equals("/")) path = "/index.html";
        File file = new File(root + path).getCanonicalFile();

        if (!file.isFile()) {
          Utils.sendRes(app, 404);
        } else {
          String mime = "text/html";
          if(path.substring(path.length()-3).equals(".js")) mime = "application/javascript";
          if(path.substring(path.length()-4).equals(".css")) mime = "text/css";

          Headers h = app.getResponseHeaders();
          h.set("Content-Type", mime);
          app.sendResponseHeaders(200, 0);
          OutputStream os = app.getResponseBody();
          FileInputStream fs = new FileInputStream(file);
          final byte[] buffer = new byte[0x10000];
          int count = 0;
          while ((count = fs.read(buffer)) >= 0) {
            os.write(buffer,0,count);
          }
          fs.close();
          os.close();
        }  
    }
}
