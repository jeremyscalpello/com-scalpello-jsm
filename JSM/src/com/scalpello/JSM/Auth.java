package com.scalpello.JSM;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

class Auth implements HttpHandler {
	private static String username = "jsm";
	private static String password = "project";
	private static ArrayList<Session> sessions = new ArrayList<Session>();
    public void handle (HttpExchange app) throws IOException {
		Map <String,String> params = Utils.parseParams(app.getRequestURI().getQuery());
		String method = app.getRequestMethod();
		if (method.equals("POST")) {
			if (params.get("username") != null && params.get("password") != null) {
				if (params.get("username").equals(username) && params.get("password").equals(password)) {
					Session temp = new Session();
					sessions.add(temp);
					Utils.setCookie(app, "jsm_auth", temp.id);
					Utils.sendRes(app, 200);
				} else {
					Utils.sendRes(app, 401);
				}
			} else {
				Utils.sendRes(app, 400);
			}
		} else if (method.equals("DELETE")) {
			Auth.check(app);
			Utils.expireCookie(app, "jsm_auth");
			Utils.sendRes(app, 200);
		} else {
			Utils.sendRes(app, 400);
		}
    }
    public static void check (HttpExchange app) throws IOException {
    	Map <String,String> cookies = Utils.parseCookies(app.getRequestHeaders().get("Cookie"));
    	String loginUrl = "/login.html";
    	String authCookie = cookies.get("jsm_auth");
    	if (authCookie != null) {
    		Iterator<Session> it = sessions.iterator();
    		while(it.hasNext()) {
    		    Session item = it.next();
    		    if (item.id.equals(authCookie)) {
    		    	return;
    		    }
    		}
    		Utils.sendRedirect(app, loginUrl);
    		return;
    	} else {
    		Utils.sendRedirect(app, loginUrl);
    		return;
    	}
    }
}
