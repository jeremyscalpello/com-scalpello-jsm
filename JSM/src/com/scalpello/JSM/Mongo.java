package com.scalpello.JSM;

import com.mongodb.*;

public class Mongo {
	public static DB db () throws Exception {
		MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
		DB conn = mongoClient.getDB("JSM");
		return conn;
	}
}
