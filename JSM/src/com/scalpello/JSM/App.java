package com.scalpello.JSM;

import java.net.InetSocketAddress;
import com.sun.net.httpserver.*;

class App {

	public static void main (String[] Args) throws Exception {
		HttpServer app = HttpServer.create(new InetSocketAddress(8000), 0);
		app.setExecutor(null);
		app.createContext("/api/students", new Student());
		app.createContext("/api/auth", new Auth());
		app.createContext("/", new StaticFiles());
		app.start();
		new Dialog();
	    System.out.println("Listening On TCP:8000");
	}

}
