package com.scalpello.JSM;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

class Dialog extends JFrame implements ActionListener {
	private static final long serialVersionUID = -7794288674936787803L;
	Dialog() {
		setSize(250,125);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setTitle("JSM Launcher");

		JButton launchBTN = new JButton("<html><body style='text-align:center'>Launch Java Student Manager<br>Make Sure MongoDB Is Running</body></html>");
		launchBTN.setBounds(0, 0, 250, 102);
		launchBTN.addActionListener(this);
		add(launchBTN);

		setVisible(true);
	}
	public void actionPerformed(ActionEvent e) {
		try {
			Utils.openUrl("http://localhost:8000");
		} catch (Exception e1) {
			System.out.println("Could Not Launch JSM");
		}
	}
}
