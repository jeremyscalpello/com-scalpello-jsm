package com.scalpello.JSM;

import java.awt.Desktop;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

public class Utils {

	public static void sendRes(HttpExchange app, String response) throws IOException {
		Headers h = app.getResponseHeaders();
	    h.set("Content-Type", "application/json");
		app.sendResponseHeaders(200, response.length());
	    OutputStream os = app.getResponseBody();
	    os.write(response.getBytes());
	    os.close();
	}

	public static void sendRes(HttpExchange app, int status) throws IOException {
	    String response = "";
	    if (status == 200) {
	        response = "{\"status\": \"OK\"}";
	    } else if (status == 384) {
	        response = "{\"status\": \"Not Modified\"}";
	    } else if (status == 400) {
	        response = "{\"status\": \"Bad Request\"}";
	    } else if (status == 401) {
	        response = "{\"status\": \"Unauthorized\"}";
	    } else if (status == 404) {
	        response = "{\"status\": \"Not Found\"}";
	    } else if (status == 500) {
	        response = "{\"status\": \"Server Error\"}";
	    } else {
	        response = "{}";
	    }
		Headers h = app.getResponseHeaders();
	    h.set("Content-Type", "application/json");
	    app.sendResponseHeaders(status, response.length());
	    OutputStream os = app.getResponseBody();
	    os.write(response.getBytes());
	    os.close();
	}

	public static void setCookie(HttpExchange app, String name, String value) throws IOException {
		Headers h = app.getResponseHeaders();
	    h.set("Set-Cookie", name + "=" + value + "; path=/");
	}

	public static void expireCookie(HttpExchange app, String name) throws IOException {
		Headers h = app.getResponseHeaders();
	    h.set("Set-Cookie", name + "=expired; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT");
	}

	public static void sendRedirect(HttpExchange app, String url) throws IOException {
		Headers h = app.getResponseHeaders();
	    h.set("Location", url);
	    String response = "Redirecting";
	    app.sendResponseHeaders(301, response.length());
	    OutputStream os = app.getResponseBody();
	    os.write(response.getBytes());
	    os.close();
	}

	public static Map<String, String> parseParams(String query) {
		if (query == null) {
			query = "";
		}
	    Map<String, String> result = new HashMap<String, String>();
	    for (String param : query.split("&")) {
	      String pair[] = param.split("=");
	      if (pair.length>1) {
	        result.put(pair[0], pair[1]);
	      }else{
	        result.put(pair[0], "");
	      }
	    }
	    return result;
	}

	public static Map<String, String> parseCookies(List<String> list) {
		String header;
		if (list == null) {
			header = "";
		} else {
			header = list.toString();
			header = header.substring(1, header.length()-1);
		}

	    Map<String, String> result = new HashMap<String, String>();
	    for (String param : header.split("; ")) {
	      String pair[] = param.split("=");
	      if (pair.length>1) {
	        result.put(pair[0], pair[1]);
	      }else{
	        result.put(pair[0], "");
	      }
	    }
	    return result;
	}

	public static String streamToString(java.io.InputStream is) {
	    @SuppressWarnings("resource")
		Scanner s = new Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}

	public static void openUrl(String url) throws IOException, URISyntaxException {
		URI uri = new URI(url);
		Desktop.getDesktop().browse(uri);
	}

}
