var app = angular.module('JSM', [
    'ngResource',
    'ngRoute',
    'ui.bootstrap',
    'angularUtils.directives.dirPagination',
    'angular-loading-bar',
    'ui-notification'
]);

app.config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'main'
            })
            .when('/students', {
                templateUrl: 'views/students.list.html',
                controller: 'students'
            })
            .when('/students.create', {
                templateUrl: 'views/students.create.html',
                controller: 'students'
            })
            .when('/students.edit/:id', {
                templateUrl: 'views/students.edit.html',
                controller: 'students'
            })
            .when('/rfid', {
                templateUrl: 'views/rfid.html',
                controller: 'rfid'
            })
            .otherwise({
                redirectTo: '/'
            });
    })
    .config(function(NotificationProvider) {
        NotificationProvider.setOptions({
            delay: 15000
        });
    });
