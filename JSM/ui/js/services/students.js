'use strict';

app.factory('Students', function ($resource) {
    return $resource('/api/students/', {
        id: '@_id.$oid'
    }, {
        update: {
            method: 'PUT'
        }
    });
});
