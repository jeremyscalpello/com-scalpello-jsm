app.controller('main', function ($scope, $http) {
    $scope.logOut = function () {
        $http.delete('/api/auth')
            .success(function () {
                window.location.href="/login.html";
            });
    };
});
