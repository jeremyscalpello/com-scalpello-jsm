app.controller('students', function ($scope, $location, $routeParams, Students) {
    $scope.getStudents = function () {
        Students.query(function (res) {
            $scope.students = res;
        });
    };

    $scope.getOne = function () {
        Students.get({
            id: $routeParams.id
        }, function (res) {
            $scope.student = res;
        });
    };

    $scope.create = function (props) {
        var student = new Students(props);
        student.$save(function (res) {
            $location.path('/students');
        });
    };

    $scope.remove = function (item) {
        item.$remove(function (res) {
            $scope.getStudents();
        });
    };

    $scope.update = function (item) {
        item.$update(function (res) {
            $location.path('/students');
        });
    };

});
