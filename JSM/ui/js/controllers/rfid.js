app.controller('rfid', function ($scope, $http, $location, Students, $timeout) {

    Students.query(function (res) {
        var students = res;

        var rfidcode = '';
        document.addEventListener('keypress', function (e) {
            if ($location.path().substring(0, 5) == "/rfid" && !$scope.timer) {
                if (e.charCode == 13) {
                    console.log('student card: ' + rfidcode);
                    for (var i = 0; i < students.length; i++) {
                        if (students[i].rfid == rfidcode) {
                            $scope.student = students[i];
                            $scope.student.exists = true;
                        } else {
                            $scope.student = {
                                exists: false
                            };
                        }
                    }
                    $scope.$apply();
                    rfidcode = '';
                    $scope.timer = $timeout(function () {
                        $scope.student = null;
                        $scope.timer = null;
                    }, 5000);
                } else rfidcode += String.fromCharCode(e.charCode);
            }
        }, true);

    });

});
